package ru.mihaylov.words.di

import androidx.lifecycle.ViewModelProvider
import dagger.Module
import dagger.Provides
import ru.mihaylov.onlinetv.ViewModelFactory
import ru.mihaylov.onlinetv.model.data.repository.DefaultMoviesRepository
import ru.mihaylov.onlinetv.model.data.repository.MoviesRepository
import ru.mihaylov.onlinetv.model.data.server.MovieApi
import javax.inject.Singleton

@Module
class ApplicationModule {

    @Provides
    @Singleton
    fun moviesRepository(movieApi: MovieApi): MoviesRepository {
        return DefaultMoviesRepository(movieApi)
    }

    @Provides
    @Singleton
    fun viewModelFactory(moviesRepository: MoviesRepository) : ViewModelProvider.Factory {
        return ViewModelFactory(moviesRepository)
    }

}