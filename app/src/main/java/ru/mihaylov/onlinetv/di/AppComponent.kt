package ru.mihaylov.words.di

import dagger.Component
import ru.mihaylov.onlinetv.ui.main.MoviesFragment
import javax.inject.Singleton

@Singleton
@Component(modules = [ApiModule::class, ApplicationModule::class])
interface AppComponent {

    fun inject(moviesFragment: MoviesFragment)

}