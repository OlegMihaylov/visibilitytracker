package ru.mihaylov.onlinetv.entity

data class MoviesShelf(
    val name: String,
    val movies: List<Movie>
)