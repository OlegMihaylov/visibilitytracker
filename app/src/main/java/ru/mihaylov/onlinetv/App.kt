package ru.mihaylov.onlinetv

import android.app.Application
import ru.mihaylov.words.di.ApiModule
import ru.mihaylov.words.di.AppComponent
import ru.mihaylov.words.di.DaggerAppComponent

class App : Application() {

    override fun onCreate() {
        super.onCreate()
        appComponent = DaggerAppComponent.builder()
            .apiModule(ApiModule(this))
            .build()
    }

    companion object {
        lateinit var appComponent: AppComponent
    }
}