package ru.mihaylov.onlinetv

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import ru.mihaylov.onlinetv.ui.main.MoviesFragment

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.main_activity)
        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction()
                    .replace(R.id.container, MoviesFragment.newInstance())
                    .commitNow()
        }
    }
}