package ru.mihaylov.onlinetv.ui.main

import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import ru.mihaylov.onlinetv.R
import ru.mihaylov.onlinetv.entity.Movie
import ru.mihaylov.onlinetv.ui.common.TrackingInfo
import ru.mihaylov.onlinetv.ui.common.VisibilityTracker
import ru.mihaylov.onlinetv.ui.common.inflate

class MoviesAdapter(private val visibilityTracker: VisibilityTracker) : ListAdapter<Movie, MovieViewHolder>(DIFF_CALLBACK) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MovieViewHolder {
        val view = parent.inflate(R.layout.movie_item, false)
        return MovieViewHolder(view)
    }

    override fun onBindViewHolder(holder: MovieViewHolder, position: Int) {
        val movie = getItem(position)
        holder.bind(movie)
        visibilityTracker.addView(holder.itemView, TrackingInfo("Показан фильм ${movie.title}"))
    }

    public override fun getItem(position: Int): Movie {
        return super.getItem(position)
    }

    companion object {
        private val DIFF_CALLBACK = object : DiffUtil.ItemCallback<Movie>() {
            override fun areItemsTheSame(oldItem: Movie, newItem: Movie): Boolean {
                return oldItem == newItem
            }

            override fun areContentsTheSame(oldItem: Movie, newItem: Movie): Boolean {
                return oldItem == newItem
            }
        }
    }

}