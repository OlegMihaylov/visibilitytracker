package ru.mihaylov.onlinetv.ui.main

import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import ru.mihaylov.onlinetv.R
import ru.mihaylov.onlinetv.entity.MoviesShelf
import ru.mihaylov.onlinetv.ui.common.TrackingInfo
import ru.mihaylov.onlinetv.ui.common.VisibilityTracker
import ru.mihaylov.onlinetv.ui.common.inflate

class MoviesShelfListAdapter(private val visibilityTracker: VisibilityTracker) : ListAdapter<MoviesShelf, MoviesShelfViewHolder>(DIFF_CALLBACK) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MoviesShelfViewHolder {
        val view = parent.inflate(R.layout.movies_shelf_item, false)
        return MoviesShelfViewHolder(view, visibilityTracker)
    }

    override fun onBindViewHolder(holder: MoviesShelfViewHolder, position: Int) {
        val movieShelf = getItem(position)
        holder.bind(movieShelf)
        visibilityTracker.addView(holder.itemView, TrackingInfo("Показана полка ${movieShelf.name}"))
    }

    public override fun getItem(position: Int): MoviesShelf {
        return super.getItem(position)
    }

    companion object {
        private val DIFF_CALLBACK = object : DiffUtil.ItemCallback<MoviesShelf>() {
            override fun areItemsTheSame(oldItem: MoviesShelf, newItem: MoviesShelf): Boolean {
                return oldItem.name == newItem.name
            }

            override fun areContentsTheSame(oldItem: MoviesShelf, newItem: MoviesShelf): Boolean {
                return oldItem == newItem
            }
        }
    }

}