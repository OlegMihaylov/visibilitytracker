package ru.mihaylov.onlinetv.ui.main

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.movies_shelf_item.view.*
import ru.mihaylov.onlinetv.entity.MoviesShelf
import ru.mihaylov.onlinetv.ui.common.VisibilityTracker

class MoviesShelfViewHolder(
    view: View,
    visibilityTracker: VisibilityTracker
) : RecyclerView.ViewHolder(view) {

    private val shelfTitleView = view.shelf_title
    private val moviesAdapter = MoviesAdapter(visibilityTracker)
    private val movieListView = view.movie_list

    init {
        movieListView.adapter = moviesAdapter
    }

    fun bind(moviesShelf: MoviesShelf) {
        shelfTitleView.text = moviesShelf.name
        moviesAdapter.submitList(moviesShelf.movies)
    }

}