package ru.mihaylov.onlinetv.ui.main

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import kotlinx.android.synthetic.main.main_fragment.*
import ru.mihaylov.onlinetv.App
import ru.mihaylov.onlinetv.R
import ru.mihaylov.onlinetv.ui.common.VisibilityTracker
import javax.inject.Inject

class MoviesFragment : Fragment() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private val viewModel by viewModels<MoviesViewModel> { viewModelFactory }
    private lateinit var visibilityTracker: VisibilityTracker
    private lateinit var moviesShelfAdapter: MoviesShelfListAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        App.appComponent.inject(this)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View {
        return inflater.inflate(R.layout.main_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        initializeRecyclerView()
        initializeDataObservers()
    }

    override fun onStart() {
        super.onStart()
        visibilityTracker.startTracking()
    }

    override fun onStop() {
        visibilityTracker.stopTracking()
        super.onStop()
    }

    private fun initializeRecyclerView() {
        visibilityTracker = VisibilityTracker({ trackingInfo ->
            val toast = Toast.makeText(context, trackingInfo.name, Toast.LENGTH_SHORT)
            toast.show()
        })
        moviesShelfAdapter = MoviesShelfListAdapter(visibilityTracker)
        movie_list.adapter = moviesShelfAdapter
    }

    private fun initializeDataObservers() {
        viewModel.isLoading.observe(viewLifecycleOwner, Observer {
            loader_block.visibility = if (it) View.VISIBLE else View.GONE
        })
        viewModel.isError.observe(viewLifecycleOwner, Observer {
            load_error_block.visibility = if (it) View.VISIBLE else View.GONE
        })
        viewModel.movieListByYear.observe(viewLifecycleOwner, Observer {
            moviesShelfAdapter.submitList(it)
        })

    }

    companion object {
        fun newInstance() = MoviesFragment()
    }

}