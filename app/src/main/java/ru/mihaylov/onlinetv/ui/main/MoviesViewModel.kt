package ru.mihaylov.onlinetv.ui.main

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.launch
import ru.mihaylov.onlinetv.entity.MoviesShelf
import ru.mihaylov.onlinetv.model.data.repository.MoviesRepository

class MoviesViewModel(
    private val moviesRepository: MoviesRepository
) : ViewModel() {

    val movieListByYear = MutableLiveData<List<MoviesShelf>>()
    val isLoading = MutableLiveData(false)
    val isError = MutableLiveData(false)

    init {
        loadMovieList()
    }

    private fun loadMovieList() {
        isLoading.value = true
        viewModelScope.launch {
            try {
                val movies = moviesRepository.getMainPage()
                val byYears = movies
                    .map { MoviesShelf(it.title, it.movieList) }
                movieListByYear.value = byYears
                isLoading.value = false
            } catch (ignore: Exception) {
                isLoading.value = false
                isError.value = true
            }
        }
    }

}