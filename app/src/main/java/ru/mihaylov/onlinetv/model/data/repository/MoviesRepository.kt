package ru.mihaylov.onlinetv.model.data.repository

import ru.mihaylov.onlinetv.entity.MainPageCategory
import ru.mihaylov.onlinetv.entity.Movie

interface MoviesRepository {

    suspend fun getMainPage(): List<MainPageCategory>

    suspend fun getMovieInCategory(): List<Movie>

}