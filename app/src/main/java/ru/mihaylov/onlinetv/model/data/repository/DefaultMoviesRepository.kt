package ru.mihaylov.onlinetv.model.data.repository

import ru.mihaylov.onlinetv.entity.MainPageCategory
import ru.mihaylov.onlinetv.entity.Movie
import ru.mihaylov.onlinetv.model.data.server.MovieApi


class DefaultMoviesRepository(private val movieApi: MovieApi): MoviesRepository {

    override suspend fun getMainPage(): List<MainPageCategory> {
        return movieApi.getMain()
            .map { it.toEntity() }
    }

    override suspend fun getMovieInCategory(): List<Movie> {
        return movieApi.getMain()
            .flatMap { it.movieList }
            .map { it.toEntity() }
    }
}