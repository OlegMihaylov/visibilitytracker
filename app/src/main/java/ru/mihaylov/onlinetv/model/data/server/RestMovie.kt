package ru.mihaylov.onlinetv.model.data.server

import com.google.gson.annotations.SerializedName
import ru.mihaylov.onlinetv.entity.Movie

data class RestMovie(
    val id: Long,
    val title: String,
    @SerializedName("image_url") val imageUrl: String
) {
    fun toEntity(): Movie {
        return Movie(id, title, imageUrl, rating = 7.8f, minAge = 12)
    }
}
