package ru.mihaylov.onlinetv.model.data.server

import retrofit2.http.GET

interface MovieApi {

    @GET("main")
    suspend fun getMain() : List<RestMainPageCategory>

}