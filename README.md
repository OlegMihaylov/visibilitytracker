# VisibilityTracker

Приложение для проверки трекера видимости вью. Основной класс [VisibilityTracker](../app/src/main/java/ru/mihaylov/onlinetv/ui/common/VisibilityTracker.kt)

# Задача

Необходимо отслеживать события просмотра различных View пользователями. View считается просмотренной, если выполнены следующие условия
- она была видима на 80%
- отображалась на экране больше заданного времени (в текущем примере 1500 мс)

# Решение

- для решения задачи создается VisibilityTracker, класс, который хранит в себе список всех View для отслеживания в рамках одной страницы.
- в качестве основного параметра VisibilityTracker-у передается родительская View в рамках которой необходимо отслеживать видимость других вью.
- у родительской View мы вешаем OnPreDrawListener, который будет нас оповещать о том, что View будет перересована.  
- при вызовах данного листенера мы планируем проверку видимости всех отслеживаемых View
- для того чтобы не выполнять проверку слишком часто, вызываем ее через Handler.postDelayed с таймаутом в 100 мс.
- в рамках проверки пробегаемся по всем отслеживаемым View, проверяем их текущую видимость с помощью метода `VisibilityTracker.isVisible`
- получаем список видимих и невидимых в текущий момент вьюх
- проходим по списку видимых вью и смотрим была ли вью видима до этого, если нет, то сохраняем ее в мапе видимых вью с параметром времени после которого можно посылать событие, что вью просмотрена слушателю.
- проходим по мапе отслеживаемых вью со временем. Для каждой вью проверяем можно ли уже посылать событие, что она просмотрена.
- проходим по мапе невидимых вью и удаляем их всех из отслеживаемых. То есть если вью была видима, мы еще не породили событие, так как время не прошло, то в этом месте мы удалим ее из мапы.

# Особенности решения
- проверка видимости происходит при перерисовке View. Если view, после первичной отрисовки была статична, то собития не будут порождеы. Для того, чтобы они все же появились реализован метод stopTracking, который необходимо вызывать при уходе со страницы.
- добавление отслеживаемых View происходит в адаптерах RecyclerView. См `MoviesShelfListAdapter.onBindViewHolder`. При переиспользовании View для отображения другого элемента в списке, она будет помещена в трекер с другими отслеживаемыми данными. Таким образом в трекере не должно быть больше View, чем требуется RecyclerView для работы. Они в нем так же будут переиспользоваться. 
- можно использовать один VisibilityTracker для отслеживания разных типов View на странице. Как полок с фильмами, так и карточек контента. Для этого необходимо будет сделать TrackingInfo интерфейсом и передавать разные реализации отслеживаемых данных.
- если необходима отправка одного отслеживаемого события в рамках сессии, то такую проверку необходимо делать уже в слое бизнес логики, а не в данном трекере.